﻿using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using EmptyProject.Extensions;
using EmptyProject.Models.Models;
using EmptyProject.ModelViews;

namespace EmptyProject.Controllers
{
    public class GlobalSettingsSingletonController : BindableBaseImpl
    {
        /// <summary>
        /// VERSION APP
        /// </summary>
        public const decimal VersionApp = (decimal)1.0;

        public static GlobalSettingsSingletonController Instance { get; set; } = new GlobalSettingsSingletonController();

        private static GlobalSettingsFileSingletonController FileSingletonGlobalSettings { get;} = GlobalSettingsFileSingletonController.Instance;
        

        private List<string> _allTypy = new List<string>() { "Windows Authentication", "SQL Server Authentication" };

        private int _infoMailBefore = 3;
        private int _infoMailAfter;
        private int _infoMailAfterRepeat = 1;
        private decimal _sumaNiezaplaconychFaktur;
        private int _liczbaKontrahentow;
        private int _liczbaDokumentow;
        private int _rowPerPage;
        private bool _isLicense;
        private LicenseManagerController.LicencjaType _licenseType = LicenseManagerController.LicencjaType.Brak;
        private string _dataWygasnieciaLicencji;
        private List<string> _drukarki;
        private bool _isTaskOpen;
        private object _taskContent;
        private bool _isActive;


       // public decimal SelectedIdAdresuFirmy { get; set; } = 0;

        public bool SelectedZalaczFv { get; set; }

        public readonly string NameTaskScheduler = "Empty project IMAG";

        private GlobalSettingsSingletonController()
        {
            //Singleton 
        }


        public bool IsZamowienie
        {
            get { return FileSingletonGlobalSettings.IsZamowienie; }

            set
            {
                FileSingletonGlobalSettings.IsZamowienie = value;
                OnPropertyChanged();
            }
        }

        public bool IsDokumentHandlowy
        {
            get { return FileSingletonGlobalSettings.IsDokumentHandlowy; }

            set
            {
                FileSingletonGlobalSettings.IsDokumentHandlowy = value;
                OnPropertyChanged();
            }
        }
        public bool IsDoumentMagazynowy
        {
            get { return FileSingletonGlobalSettings.IsDoumentMagazynowy; }

            set
            {
                FileSingletonGlobalSettings.IsDoumentMagazynowy = value;
                OnPropertyChanged();
            }
        }

        public string NadawcaFirma
        {
            get { return FileSingletonGlobalSettings.FirmaNadawca; }
            set
            {
                FileSingletonGlobalSettings.FirmaNadawca = value;
                OnPropertyChanged();

            }
        }
        public string ImieNazwiskoNadawca
        {
            get { return FileSingletonGlobalSettings.ImieNazwiskoNadawca; }
            set
            {
                FileSingletonGlobalSettings.ImieNazwiskoNadawca = value;
                OnPropertyChanged();

            }
        }
        public string UlicaNadawca
        {
            get { return FileSingletonGlobalSettings.UlicaNadawca; }
            set
            {
                FileSingletonGlobalSettings.UlicaNadawca = value;
                OnPropertyChanged();

            }
        }
        public string KodPocztowyNadawca
        {
            get { return FileSingletonGlobalSettings.KodPocztowyNadawca; }
            set
            {
                FileSingletonGlobalSettings.KodPocztowyNadawca = value;
                OnPropertyChanged();

            }
        }
        public string MiejscowoscNadawca
        {
            get { return FileSingletonGlobalSettings.MiejscowoscNadawca; }
            set
            {
                FileSingletonGlobalSettings.MiejscowoscNadawca = value;
                OnPropertyChanged();

            }
        }
        public string KrajNadawca
        {
            get { return FileSingletonGlobalSettings.KrajNadawca; }
            set
            {
                FileSingletonGlobalSettings.KrajNadawca = value;
                OnPropertyChanged();

            }
        }
       

        public bool PrintAfterGenerateReport
        {
            get { return FileSingletonGlobalSettings.PrintActive; }
            set
            {
                FileSingletonGlobalSettings.PrintActive = value;
                OnPropertyChanged();
             
            }
        }
        public int IdDokWz { get; set; }
        public int IdZam { get; set; }
        public int IdDokHan { get; set; }
  
        public string DrukarkaRaportow
        {
            get { return FileSingletonGlobalSettings.Drukarka; }
            set
            {
                FileSingletonGlobalSettings.Drukarka = value;
                OnPropertyChanged();
        
            }
        }

        public string SciezkaZapisuEtykiety
        {
            get { return FileSingletonGlobalSettings.SciezkaZapisuEtykiet; }
            set
            {
                FileSingletonGlobalSettings.SciezkaZapisuEtykiet = value;
                OnPropertyChanged(() => SciezkaZapisuEtykiety);

            }
        }

        public string CmrRaportPath
        {
            get { return FileSingletonGlobalSettings.CmrRaportPath; }
            set
            {
                FileSingletonGlobalSettings.CmrRaportPath = value;
                OnPropertyChanged();
             
            }
        }
        public decimal SelectedDaneNadawcy
        {
            get { return FileSingletonGlobalSettings.SelectedDaneNadawcy; }
            set
            {
               FileSingletonGlobalSettings.SelectedDaneNadawcy = value;
                OnPropertyChanged();
               
            }
        }

        public FieldsFromEnum SelectedDaneOdbiorcy
        {
            get { return FileSingletonGlobalSettings.SelectedDaneOdbiorcy; }
            set
            {
                FileSingletonGlobalSettings.SelectedDaneOdbiorcy = value;
                OnPropertyChanged();
            
            }
        }

        //
        public string TextBrakLicencji = "Brak licencji - nie można wykonać zadania!";
        private string _cmrRaportPath;
        private string _drukarkaRaportow;
        private bool _printAfterGenerateReport;
        private bool _pobranie;

        public StartAppType StartAppType { get; set; }

        public List<string> AllTypy
        {
            get { return _allTypy; }
            set
            {
                _allTypy = value;
                OnPropertyChanged(() => AllTypy);
            }
        }

        public int InfoMailBefore
        {
            get { return _infoMailBefore; }
            set
            {
                _infoMailBefore = value;
                OnPropertyChanged(() => InfoMailBefore);
            }
        }

        public int InfoMailAfter
        {
            get { return _infoMailAfter; }
            set
            {
                _infoMailAfter = value;
                OnPropertyChanged(() => InfoMailAfter);
            }
        }

        public int InfoMailAfterRepeat
        {
            get { return _infoMailAfterRepeat; }
            set
            {
                _infoMailAfterRepeat = value;
                OnPropertyChanged(() => InfoMailAfterRepeat);
            }
        }

        public decimal SumaNiezaplaconychFaktur
        {
            get { return _sumaNiezaplaconychFaktur; }
            set
            {
                _sumaNiezaplaconychFaktur = value;
                OnPropertyChanged(() => SumaNiezaplaconychFaktur);
            }
        }

        public int LiczbaKontrahentow
        {
            get { return _liczbaKontrahentow; }
            set
            {
                _liczbaKontrahentow = value;
                OnPropertyChanged(() => LiczbaKontrahentow);
            }
        }

        public int LiczbaDokumentow
        {
            get { return _liczbaDokumentow; }
            set
            {
                _liczbaDokumentow = value;
                OnPropertyChanged(() => LiczbaDokumentow);
            }
        }

        public int RowPerPage
        {
            get { return _rowPerPage; }
            set
            {
                _rowPerPage = value;
                OnPropertyChanged(() => RowPerPage);
            }
        }

        public string DataWygasnieciaLicencji
        {
            get { return _dataWygasnieciaLicencji; }
            set
            {
                _dataWygasnieciaLicencji = value;
                OnPropertyChanged(() => DataWygasnieciaLicencji);
            }
        }

        public List<string> Drukarki
        {
            get { return PrinterSettings.InstalledPrinters.Cast<string>().ToList(); }
            set
            {
                _drukarki = value;
                OnPropertyChanged(() => Drukarki);
            }
        }

        public bool Pobranie
        {
            get { return _pobranie; }
            set
            {
                if (_pobranie == value) return;
                _pobranie = value;
                OnPropertyChanged();
            }
        }
        public bool IsLicense
        {
            get { return _isLicense; }
            set
            {
                if (_isLicense == value) return;
                _isLicense = value;
                OnPropertyChanged();
            }
        }

        public LicenseManagerController.LicencjaType LicenseType
        {
            get { return _licenseType; }
            set
            {
                if (_licenseType == value) return;
                _licenseType = value;
                OnPropertyChanged();
            }
        }

        public bool IsTaskOpen
        {
            get { return _isTaskOpen; }
            set
            {
                if (_isTaskOpen == value) return;
                _isTaskOpen = value;
                OnPropertyChanged();
            }
        }

        public object TaskContent
        {
            get { return _taskContent; }
            set
            {
                if (_taskContent == value) return;
                _taskContent = value;
                OnPropertyChanged();
            }
        }

        public bool BreakAutoTask { get; set; }

        public bool IsActiveTaskScheduler
        {
            get { return FileSingletonGlobalSettings.IsActiveTaskScheduler; }
            set
            {
                FileSingletonGlobalSettings.IsActiveTaskScheduler = value;
                OnPropertyChanged();
            }
        }

        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                OnPropertyChanged();
            }
        }

        public int Interval
        {
            get { return FileSingletonGlobalSettings.Interval; }
            set
            {
                FileSingletonGlobalSettings.Interval = value;
                OnPropertyChanged();
            }
        }

        //public string Drukarka
        //{
        //    get { return FileSingletonGlobalSettings.Drukarka; }
        //    set
        //    {
        //        FileSingletonGlobalSettings.Drukarka = value;
        //        OnPropertyChanged();
        //    }
        //}

        public bool PrintActive
        {
            get { return FileSingletonGlobalSettings.PrintActive; }
            set
            {
                FileSingletonGlobalSettings.PrintActive = value;
                OnPropertyChanged();
            }
        }

        public string NumerLicencji
        {
            get { return FileSingletonGlobalSettings.NumerLicencji; }
            set
            {
                FileSingletonGlobalSettings.NumerLicencji = value;
                OnPropertyChanged();
            }
        }

       
    }
}