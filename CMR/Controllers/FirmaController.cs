﻿using System;
using System.Collections.Generic;
using EmptyProject.Extensions;
using EmptyProject.Models.Models;
using EmptyProject.Models.Repository;

namespace EmptyProject.Controllers
{
    class FirmaController
    {

        public static AdresModel GetById(decimal idAdresuFirma)
        {
            try
            {
                var repo = new FirmaRepository(new DatabaseClientEntities(DataBaseController.Instance.DbConnection()));
                return repo.GetAdresFirmyByIdFirma(idAdresuFirma);
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                return new AdresModel();
            }
        }

        public static Dictionary<decimal, string> GetAllAdresyFirmyAdd0()
        {
            try
            {
                var repo = new FirmaRepository(new DatabaseClientEntities(DataBaseController.Instance.DbConnection()));
                var req = repo.GetAllAdresyFirmy();
                req.Add(0, "Nie pobieraj");
                return req;
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                return new Dictionary<decimal, string>();
            }
        }
    }
}
