﻿using System;
using EmptyProject.Extensions;
using EmptyProject.Models.Models;

namespace EmptyProject.Controllers
{
    [Serializable]
    public class GlobalSettingsFileSingletonController
    {
        public static GlobalSettingsFileSingletonController Instance { get; set; } = GetSetting();

        private string _auth = "SQL";
        private string _login = "sa";
        private string _password = "Wapro3000";
        private string _dbServerName = @"LAPDB1\SQLEXPRESS2014";
        private string _dbName = "WAPRO_DEMO";
        private string _drukarka;
        private bool _isActiveTaskScheduler;
        private string _numerLicencji = "";

        private bool _printActive;
        private int _interval = 1;

        private FieldsFromEnum _selectedDaneOdbiorcy;
        private decimal _selectedDaneNadawcy = 0;

        private string _cmrRaportPath;
        private string _firmaNadawca;
        private string _imieNazwiskoNadawca;
        private string _ulicaNadawca;
        private string _kodPocztowyNadawca;
        private string _miejscowoscNadawca;
        private string _krajNadawca;
        private string _sciezkaZapisuEtykiet;
        private bool _isZamowienie;
        private bool _isDokumentHandlowy;
        private bool _isdokumentMagazynowy;

        public bool IsZamowienie
        {
            get { return _isZamowienie; }

            set
            {
                _isZamowienie = value;
                Serialize();
            }
        }

        public bool IsDokumentHandlowy
        {
            get { return _isDokumentHandlowy; }

            set
            {
                _isDokumentHandlowy = value;
                Serialize();
            }
        }
        public bool IsDoumentMagazynowy
        {
            get { return _isdokumentMagazynowy; }

            set
            {
                _isdokumentMagazynowy = value;
                Serialize();
            }
        }

        public string FirmaNadawca
        {
            get { return _firmaNadawca; }
            set
            {
                _firmaNadawca = value;
                Serialize();
            }
        }

        public string SciezkaZapisuEtykiet
        {
            get { return _sciezkaZapisuEtykiet; }
            set
            {
                _sciezkaZapisuEtykiet = value;
                Serialize();
                
            }
        }

        public string ImieNazwiskoNadawca
        {
            get { return _imieNazwiskoNadawca; }
            set
            {
                _imieNazwiskoNadawca = value;
                Serialize();
            }
        }
        public string UlicaNadawca
        {
            get { return _ulicaNadawca; }
            set
            {
                _ulicaNadawca = value;
                Serialize();
            }
        }
        public string KodPocztowyNadawca
        {
            get { return _kodPocztowyNadawca; }
            set
            {
                _kodPocztowyNadawca = value;
                Serialize();
            }
        }
        public string MiejscowoscNadawca
        {
            get { return _miejscowoscNadawca; }
            set
            {
                _miejscowoscNadawca = value;
                Serialize();
            }
        }
        public string KrajNadawca
        {
            get { return _krajNadawca; }
            set
            {
                _krajNadawca = value;
                Serialize();
            }
        }


        public string Auth
        {
            get { return _auth; }
            set
            {
                _auth = value;
                Serialize();
            }
        }

        public string Login
        {
            get { return _login; }
            set
            {
                _login = value;
                Serialize();
            }
        }

        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                Serialize();
            }
        }
        public string CmrRaportPath
        {
            get { return _cmrRaportPath; }
            set
            {
                _cmrRaportPath = value;
                Serialize();

            }
        }
        public FieldsFromEnum SelectedDaneOdbiorcy
        {
            get { return _selectedDaneOdbiorcy;}
            set
            {
                _selectedDaneOdbiorcy = value;
                Serialize();
            }
        }

        public decimal SelectedDaneNadawcy 
        {
            get { return _selectedDaneNadawcy; }
            set
            {
                _selectedDaneNadawcy = value;
                Serialize();
            }
        }
       
   

        public string ServerName
        {
            get { return _dbServerName; }
            set
            {
                _dbServerName = value;
                Serialize();
            }
        }

        public string DbName
        {
            get { return _dbName; }
            set
            {
                _dbName = value;
                Serialize();
            }
        }
      


        public string Drukarka
        {
            get { return _drukarka; }
            set
            {
                _drukarka = value;
                Serialize();
            }
        }

        public bool IsActiveTaskScheduler
        {
            get { return _isActiveTaskScheduler; }
            set
            {
                _isActiveTaskScheduler = value;
                Serialize();
            }
        }

      

        public bool PrintActive
        {
            get { return _printActive; }
            set
            {
                _printActive = value;
                Serialize();
            }
        }

        public int CrystalReportPriority { get; set; } = 1; //TODO

        public int SpedytorPriority { get; set; } = 2; //TODO

        public int Interval
        {
            get { return _interval; }
            set
            {
                _interval = value;
                Serialize();
            }
        }

        public string NumerLicencji
        {
            get { return _numerLicencji; }
            set
            {
                _numerLicencji = value;
                Serialize();
            }
        }

        private GlobalSettingsFileSingletonController() { } //Singleton

        public static GlobalSettingsFileSingletonController GetSetting()
        {

            try
            {
                var currentSettings = Deserialize(AppDomain.CurrentDomain.BaseDirectory + @"/Settings.bin");

                if (currentSettings != null)
                    return currentSettings;

                Serialize();
                return new GlobalSettingsFileSingletonController();
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                return new GlobalSettingsFileSingletonController();
            }
        }

        public static bool SetDatabaseData(GlobalSettingsFileSingletonController obj)
        {
            Serialize(obj);
            return true;
        }

        private static void Serialize(GlobalSettingsFileSingletonController obj)
        {
            BinarySerializer<GlobalSettingsFileSingletonController>.Serialize(AppDomain.CurrentDomain.BaseDirectory + @"/Settings.bin", obj);
        }

        private static void Serialize()
        {
            BinarySerializer<GlobalSettingsFileSingletonController>.Serialize(AppDomain.CurrentDomain.BaseDirectory + @"/Settings.bin", Instance);
        }

        private static GlobalSettingsFileSingletonController Deserialize(string path)
        {
            return BinarySerializer<GlobalSettingsFileSingletonController>.Deserialize(path);
        }
    }
}
