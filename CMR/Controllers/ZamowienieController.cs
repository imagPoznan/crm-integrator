﻿using System;
using System.Windows;
using EmptyProject.Extensions;
using EmptyProject.Models.Models;
using EmptyProject.Models.Repository;
using EmptyProject.ModelViews;

namespace EmptyProject.Controllers
{
    internal class ZamowienieController
    {
        public static DaneOdbiorcyModel GetComplexById(decimal id)
        {
            try
            {
                var repo =
                    new ZamowienieRepository(new DatabaseClientEntities(DataBaseController.Instance.DbConnection()));
                return repo.GetComplexById(id);
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                if (ex.Message.Equals("The underlying provider failed on Open."))
                    throw new Exception("Nie można połaczyć się z bazą danych");
                throw new Exception(ex.Message);

            }
        }
       

        public static void ZapiszNumerListu(decimal id, string numerListu)
        {
           
                var repo = new GenerujRepository(new DatabaseClientEntities(DataBaseController.Instance.DbConnection()));
                 repo.ZapiszNumerListu(id, numerListu);
          
        }

       

        public static DaneOdbiorcyModel LoadedActionAsync(DaneOdbiorcyModel zamowienieModel)
        {
            if (zamowienieModel == null)
                throw new ArgumentNullException(nameof(zamowienieModel));

            if (!GlobalSettingsSingletonController.Instance.IsLicense)
                throw new Exception(GlobalSettingsSingletonController.Instance.TextBrakLicencji);

            if (GlobalSettingsSingletonController.Instance.StartAppType == StartAppType.WfMag)
                zamowienieModel = GetComplexById(GlobalSettingsSingletonController.Instance.IdZam);

            return zamowienieModel;
        }

        public static DaneOdbiorcyModel GetZamowienieByIdAsync(
             DaneOdbiorcyModel zamowienieModel)
        {
            if (zamowienieModel == null) throw new ArgumentNullException(nameof(zamowienieModel));

            if (!GlobalSettingsSingletonController.Instance.IsLicense)
                throw new Exception(GlobalSettingsSingletonController.Instance.TextBrakLicencji);

            return GetComplexById(GlobalSettingsSingletonController.Instance.IdZam);// TODO id zam
        }
    }
}
