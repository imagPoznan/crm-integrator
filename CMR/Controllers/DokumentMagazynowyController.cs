﻿using System;
using EmptyProject.Extensions;
using EmptyProject.Models.Models;
using EmptyProject.Models.Repository;
using EmptyProject.ModelViews;

namespace EmptyProject.Controllers
{
    internal class DokumentMagazynowyController
    {
        public static DaneOdbiorcyModel GetComplexById(decimal id)
        {
            try
            {
                var repo = new DokumentMagazynowyRepository(new DatabaseClientEntities(DataBaseController.Instance.DbConnection()));
                return repo.GetComplexById(id);
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                if(ex.Message.Equals("The underlying provider failed on Open."))
                    throw new Exception("Nie można połaczyć się z bazą danych");
                throw new Exception(ex.Message);
            }

        }

        public static bool Save(DaneOdbiorcyModel dokumentMagazynowyModel)
        {
            try
            {
                var repo = new ImagCmrRepository(new DatabaseClientEntities(DataBaseController.Instance.DbConnection()));
                repo.Save(dokumentMagazynowyModel);
                return true;
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                if (ex.Message.Equals("The underlying provider failed on Open."))
                    throw new Exception("Nie można połaczyć się z bazą danych");

                throw new Exception(ex.Message);
            }
        }

        public static DaneOdbiorcyModel LoadedActionAsync(DaneOdbiorcyModel dokumentMagazynowyModel)
        {
            if (dokumentMagazynowyModel == null)
                throw new ArgumentNullException(nameof(dokumentMagazynowyModel));

            if (!GlobalSettingsSingletonController.Instance.IsLicense)
                throw new Exception(GlobalSettingsSingletonController.Instance.TextBrakLicencji);

            if (GlobalSettingsSingletonController.Instance.StartAppType == StartAppType.WfMag)
                dokumentMagazynowyModel = GetComplexById(GlobalSettingsSingletonController.Instance.IdDokWz);

            return dokumentMagazynowyModel;
        }

        public static DaneOdbiorcyModel GetDokumentMagazynowyByIdAsync(
            DaneOdbiorcyModel dokumentMagazynowyModel)
        {
            if (dokumentMagazynowyModel == null) throw new ArgumentNullException(nameof(dokumentMagazynowyModel));

            if (!GlobalSettingsSingletonController.Instance.IsLicense)
                throw new Exception(GlobalSettingsSingletonController.Instance.TextBrakLicencji);

            return GetComplexById(GlobalSettingsSingletonController.Instance.IdDokWz);
        }
    }
}
