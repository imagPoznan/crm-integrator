﻿using System;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using EmptyProject.Extensions;
using EmptyProject.Models.Models;

namespace EmptyProject.Controllers
{
    public sealed class DataBaseController
    {
        public static DataBaseController Instance { get; set; } = new DataBaseController();

        public string Auth
        {
            get { return GlobalSettingsFileSingletonController.Instance.Auth; }
            set { GlobalSettingsFileSingletonController.Instance.Auth = value; }
        }

        public string Login
        {
            get { return GlobalSettingsFileSingletonController.Instance.Login; }
            set { GlobalSettingsFileSingletonController.Instance.Login = value; }
        }

        public string Password
        {
            get { return GlobalSettingsFileSingletonController.Instance.Password; }
            set { GlobalSettingsFileSingletonController.Instance.Password = value; }
        }

        public string DbServerName
        {
            get { return GlobalSettingsFileSingletonController.Instance.ServerName; }
            set { GlobalSettingsFileSingletonController.Instance.ServerName = value; }
        }

        public string DbName
        {
            get { return GlobalSettingsFileSingletonController.Instance.DbName; }
            set { GlobalSettingsFileSingletonController.Instance.DbName = value; }
        }

        private DataBaseController()
        {
        }

        public string DbConnection()
        {
            try
            {
                var uwierzytelnienie = !Auth.Contains("SQL") ? "Integrated Security=true;" : $"User Id={Login};Password={Password};";

                string connString =
                    $@"data source={DbServerName};initial catalog={DbName};{uwierzytelnienie}MultipleActiveResultSets=True;App=EntityFramework; ";

                var esb = new EntityConnectionStringBuilder
                {
                    Metadata = "res://*/DatabaseModel.csdl|res://*/DatabaseModel.ssdl|res://*/DatabaseModel.msl",
                    Provider = "System.Data.SqlClient",
                    ProviderConnectionString = connString
                };
                return esb.ToString();
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
            }
            return "";
        }

        public string CreateConnStrWithoutEntity()
        {
            var windowsAuthString = "Windows Authentication";
            var uwierzytelnienie = !Auth.Contains("SQL") ? windowsAuthString : $"User Id={Login};Password={Password};";

            var auth = uwierzytelnienie == windowsAuthString ? "Integrated Security=true;" : $"User Id={Login};Password={Password};";


            return $"Data Source={DbServerName};Initial Catalog={DbName};{auth}";
        }

        public static bool SaveDatabaseData(DataBaseController dbObject)
        {
            try
            {
                GlobalSettingsFileSingletonController.Instance.ServerName = dbObject.DbServerName;
                GlobalSettingsFileSingletonController.Instance.DbName = dbObject.DbName;
                GlobalSettingsFileSingletonController.Instance.Auth = dbObject.Auth;
                GlobalSettingsFileSingletonController.Instance.Login = dbObject.Login;
                GlobalSettingsFileSingletonController.Instance.Password = dbObject.Password;

                return true;
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                return false;
            }

        }

        public static void Reset()
        {
            Instance = new DataBaseController();
        }

        public bool RunQueryWithoutEntity(string query)
        {
            try
            {
                using (var conn = new SqlConnection(CreateConnStrWithoutEntity()))
                {
                    conn.Open();
                    using (var cmd = new SqlCommand(query, conn))
                    {
                        if (query != null)
                            cmd.ExecuteNonQuery();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Logs.AddLogError(new Exception("QUERY:" + query + ";" + ex));
                return false;
            }
        }

        public bool CreateTables()
        {
            var boolReq = RunQueryWithoutEntity(QueryModel.Sprawdz());

            return boolReq;
        }

        public static bool SaveDatabaseDataAction(DataBaseController dataBaseSingletonController)
        {
            var resp = SaveDatabaseData(dataBaseSingletonController);
            Reset();

            return resp;
        }

        public static bool ConnectDatabase()
        {
            if (Instance == null || !Instance.CreateTables())
                throw new Exception("Wystąpił błąd podczas tworzenia tabel w bazie danych - sprawdź połaczenie z bazą danych");

            return true;

            //     if (isConnected)
            //   await ShowMessagebox(@"Baza danych jest gotowa do pracy");
        }
    }
}
