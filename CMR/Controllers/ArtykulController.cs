﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using EmptyProject.Extensions;
using EmptyProject.Models.Models;
using EmptyProject.Models.Repository;

namespace EmptyProject.Controllers
{
    class ArtykulController
    {
       

        private static decimal GetWagaArtukulu(decimal idArtykulu)
        {
            var repo = new ArtykulRepository(new DatabaseClientEntities(DataBaseController.Instance.DbConnection()));
            var arts = repo.GetById(idArtykulu);

            return arts.WAGA??0;
        }

        private static decimal? GetObjetosc(decimal idArtykulu)
        {
            var repo = new ArtykulRepository(new DatabaseClientEntities(DataBaseController.Instance.DbConnection()));
            var arts = repo.GetById(idArtykulu);

            return arts.WYMIAR_G ?? 0 * arts.WYMIAR_S ?? 0 * arts.WYMIAR_W ?? 0;
        }

        private static string GetName(FieldsFromEnum instanceSelectedDaneOdbiorcy, decimal idArtykul)
        {
            var repo = new ArtykulRepository(new DatabaseClientEntities(DataBaseController.Instance.DbConnection()));
            var arts = repo.GetById(idArtykul);

            switch (instanceSelectedDaneOdbiorcy)
            {
                case FieldsFromEnum.ArtykulNazwa:
                {
                    return arts.NAZWA;
                }
                case FieldsFromEnum.ArtukulNazwaOrg:
                    {
                        return arts.NAZWA_ORYG;
                    }
                case FieldsFromEnum.ArtukulNazwaNazwaCd:
                    {
                        return arts.NAZWA + " " + arts.NAZWA2;
                    }
                case FieldsFromEnum.ArtukulNazwaNazwaOrg:
                    {
                        return arts.NAZWA + " " + arts.NAZWA_ORYG;
                    }
                case FieldsFromEnum.ArtukulNazwaNazwaCdNazwaOrg:
                    {
                        return arts.NAZWA + " " + arts.NAZWA2+ " "+ arts.NAZWA_ORYG;
                    }
                case FieldsFromEnum.None:
                    return string.Empty;
                default:
                    throw new Exception("Nie wybrano nazwy artykulu");
            }
           
        }
    }
}
