﻿using System;
using EmptyProject.Extensions;
using EmptyProject.Models.Repository;
using EmptyProject.Web_References.LicenseManagerApi;

namespace EmptyProject.Controllers
{
    public class LicenseManagerController
    {
        public static bool GetLicense(string numerLicencji, out LicencjaType licencjaType, out DateTime? dataWygasniecia)
        {
            try
            {
                var licencja = new service {Url = "http://195.160.180.44/LicenseManager.svc?wsdl"};

                var wfmagDbId = GetWfMagDbNumber();

                if (string.IsNullOrEmpty(numerLicencji) || string.IsNullOrEmpty(wfmagDbId))
                    throw new ArgumentException();

                var response = licencja.GetLicense(numerLicencji, wfmagDbId, "EmptyProject");

                licencjaType = response != null && response.Response ? LicencjaType.Potwierdzona : LicencjaType.Brak;
                dataWygasniecia = response != null && response.DataWygasnieciaSpecified
                    ? response.DataWygasniecia
                    : null;
                return response != null && response.Response;
            }
            catch (ArgumentException ex)
            {
                Logs.AddLogError(ex);
                licencjaType = LicencjaType.Brak;
                dataWygasniecia = null;
                return false;
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                licencjaType = LicencjaType.Tymczasowa;
                dataWygasniecia = null;
                return true;
            }
         
        }

        public static bool ActiveLicense(string numerLicencji, out LicencjaType licencjaType, out string licencjaResponseDesc, out DateTime? licencjaResponseDataWygasniecia)
        {
            try
            {
                var licencja = new service { Url = "http://195.160.180.44/LicenseManager.svc?wsdl" };

                var wfmagDbId = GetWfMagDbNumber();

                if (string.IsNullOrEmpty(numerLicencji) || string.IsNullOrEmpty(wfmagDbId))
                    throw new ArgumentException();

                var response = licencja.ActiveLicense(numerLicencji, wfmagDbId, "EmptyProject");

                licencjaType = response != null && response.Response ? LicencjaType.Potwierdzona : LicencjaType.Brak;

                licencjaResponseDesc = response?.Description;
                licencjaResponseDataWygasniecia = response != null && response.DataWygasnieciaSpecified ? response.DataWygasniecia:null;

                return response != null && response.Response;
            }
            catch (ArgumentException ex)
            {
                Logs.AddLogError(ex);
                licencjaType = LicencjaType.Brak;
                licencjaResponseDesc = "Brak numeru licencji lub brak połączenia z bazą danych WF-MAG";
                licencjaResponseDataWygasniecia = null;
                return false;
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                licencjaType = LicencjaType.Tymczasowa;
                licencjaResponseDesc = "Brak połączenia z API";
                licencjaResponseDataWygasniecia = null;
                return true;
            }
        }

        private static string GetWfMagDbNumber()
        {
            try
            {
                var getWfMagDbNumber = new WaproDbRepository(new DatabaseClientEntities(DataBaseController.Instance.DbConnection()));
                var wfmagDbId = getWfMagDbNumber.GetDbId();
                return wfmagDbId;
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                return string.Empty;
            }
          
        }

        public static string CheckLicenseButtonAction()
        {
            string respDesc;
            DateTime? respDataWygasniecia = null;
            LicencjaType licencjaType;

            GlobalSettingsSingletonController.Instance.IsLicense = ActiveLicense(GlobalSettingsSingletonController.Instance.NumerLicencji, out licencjaType, out respDesc, out respDataWygasniecia);
            GlobalSettingsSingletonController.Instance.LicenseType = licencjaType;
            GlobalSettingsSingletonController.Instance.DataWygasnieciaLicencji = respDataWygasniecia?.ToLongDateString() ?? "-";

            return respDesc;
        }

        public enum LicencjaType
        {
            Potwierdzona,
            Brak,
            Tymczasowa
        }
    }
}
