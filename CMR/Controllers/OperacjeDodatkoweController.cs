﻿using System;
using EmptyProject.Extensions;
using EmptyProject.Models.Repository;

namespace EmptyProject.Controllers
{
    class OperacjeDodatkoweController
    {
        public static bool DodajDoDokumentuMagazynowego()
        {
            try
            {
                var path = System.Reflection.Assembly.GetExecutingAssembly().Location;

                var objectOpercaja = new MAG_OPERACJE_DODATKOWE()
                {
                    NAZWA = "Moduł CMR IMAG",
                    KOD_GRUPY = "DOKMAG_BRW",
                    KOD_OPERACJI = "1111_000_CMRIMAG",
                    GRUPOWANIE = 1,
                    PARAMETRY = "0001000010000",
                    SCIEZKA = path,
                    SKROT_KLAWISZOWY = String.Empty,
                    SKROT_KLAWISZOWY_KOD = 0,
                    FUNKCJA_ZEWNETRZNA = 1,
                    ODSWIEZ_LISTE = 0,
                    KASUJ_ZAZNACZENIE = 0
                };

                var repo =
                    new OperacjeDodatkoweRepository(
                        new DatabaseClientEntities(DataBaseController.Instance.DbConnection()));

                return repo.Add(objectOpercaja);
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                return false;
            }
        }

        public static bool DodajDoZamowienia()
        {
            try
            {
                
                var path = System.Reflection.Assembly.GetExecutingAssembly().Location;

                var objectOpercaja = new MAG_OPERACJE_DODATKOWE()
                {
                    NAZWA = "Empty_ProJECT",
                    KOD_GRUPY = "ZAM_BRW",
                    KOD_OPERACJI = "1111_000_EMPTYy",
                    GRUPOWANIE = 1,
                    PARAMETRY = "0001000010000",
                    SCIEZKA = path,
                    SKROT_KLAWISZOWY = String.Empty,
                    SKROT_KLAWISZOWY_KOD = 0,
                    FUNKCJA_ZEWNETRZNA = 1,
                    ODSWIEZ_LISTE = 0,
                    KASUJ_ZAZNACZENIE = 0
                };

                var repo =
                    new OperacjeDodatkoweRepository(
                        new DatabaseClientEntities(DataBaseController.Instance.DbConnection()));

                return repo.Add(objectOpercaja);
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                return false;
            }
        }
        public static bool DodajDoDokumentuHandlowego() //TODO
        {
            try
            {

                var path = System.Reflection.Assembly.GetExecutingAssembly().Location;

                var objectOpercaja = new MAG_OPERACJE_DODATKOWE()
                {
                    NAZWA = "Empty_ProJECT",
                    KOD_GRUPY = "DOKHAN_BRW",
                    KOD_OPERACJI = "1111_000_EMPTYy",
                    GRUPOWANIE = 1,
                    PARAMETRY = "0001000010000",
                    SCIEZKA = path,
                    SKROT_KLAWISZOWY = String.Empty,
                    SKROT_KLAWISZOWY_KOD = 0,
                    FUNKCJA_ZEWNETRZNA = 1,
                    ODSWIEZ_LISTE = 0,
                    KASUJ_ZAZNACZENIE = 0
                };

                var repo =
                    new OperacjeDodatkoweRepository(
                        new DatabaseClientEntities(DataBaseController.Instance.DbConnection()));

                return repo.Add(objectOpercaja);
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                return false;
            }
        }
    }
}
