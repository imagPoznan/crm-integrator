﻿using System;
using EmptyProject.Extensions;
using EmptyProject.Models.Models;
using EmptyProject.Models.Repository;
using EmptyProject.ModelViews;

namespace EmptyProject.Controllers
{
    internal class DokumentHandlowyController
    {
        public static DaneOdbiorcyModel GetComplexById(decimal id)
        {
            try
            {
                var repo = new DokumentHandlowyRepository(new DatabaseClientEntities(DataBaseController.Instance.DbConnection()));
                return repo.GetComplexById(id);
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                if (ex.Message.Equals("The underlying provider failed on Open."))
                    throw new Exception("Nie można połaczyć się z bazą danych");
                throw new Exception(ex.Message);

            }
        }


        public static DaneOdbiorcyModel LoadedActionAsync(DaneOdbiorcyModel zamowienieModel)
        {
            if (zamowienieModel == null)
                throw new ArgumentNullException(nameof(zamowienieModel));

            if (!GlobalSettingsSingletonController.Instance.IsLicense)
                throw new Exception(GlobalSettingsSingletonController.Instance.TextBrakLicencji);

            if (GlobalSettingsSingletonController.Instance.StartAppType == StartAppType.WfMag)
                zamowienieModel = GetComplexById(GlobalSettingsSingletonController.Instance.IdDokHan);

            return zamowienieModel;
        }

        public static DaneOdbiorcyModel GetDokHandlowyByIdAsync(
             DaneOdbiorcyModel zamowienieModel)
        {
            if (zamowienieModel == null) throw new ArgumentNullException(nameof(zamowienieModel));

            if (!GlobalSettingsSingletonController.Instance.IsLicense)
                throw new Exception(GlobalSettingsSingletonController.Instance.TextBrakLicencji);

            return GetComplexById(GlobalSettingsSingletonController.Instance.IdDokHan);// TODO id zam
        }
    }
}
