﻿using System;
using EmptyProject.Extensions;
using EmptyProject.Models.Models;
using EmptyProject.Models.Repository;

namespace EmptyProject.Controllers
{
    public class KontrahentController
    {
        public static AdresModel GetById(decimal id)
        {
            try
            {
                var repo = new KontrahentRepository(new DatabaseClientEntities(DataBaseController.Instance.DbConnection()));
                return repo.GetAdresById(id);
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                return new AdresModel();
            }
        }
    }
}
