﻿using System;
using EmptyProject.Extensions;
using EmptyProject.Models.Models;
using EmptyProject.Models.Repository;

namespace EmptyProject.Controllers
{
    public class MiejsceDostawyController
    {
        public static AdresModel GetAdresByIdDokMag(decimal idDokMag,decimal idKontrahenta)
        {
            try
            {
                switch (GlobalSettingsSingletonController.Instance.SelectedDaneOdbiorcy)
                {
                    case FieldsFromEnum.WzMiejsceDostawy:
                    {
                        var repo = new MiejsceDostawyRepository(new DatabaseClientEntities(DataBaseController.Instance.DbConnection()));
                        return repo.GetAdresByIdDokMag(idDokMag);
                    }
                    case FieldsFromEnum.Kontrahent:
                    {
                        var repo = new KontrahentRepository(new DatabaseClientEntities(DataBaseController.Instance.DbConnection()));
                        return repo.GetAdresById(idKontrahenta);
                        }
                        case FieldsFromEnum.MiejsceDostawyZamowienie:
                    {
                        var repo = new MiejsceDostawyRepository(new DatabaseClientEntities(DataBaseController.Instance.DbConnection()));
                        return repo.GetAdresByIdZam(idDokMag);
                    }
                    case FieldsFromEnum.None:
                        return new AdresModel();
                    default:
                        throw new Exception("Nie wybrano adresu odbiorcy");
                }
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                throw new Exception(ex.Message);
            }
        }

     
    }
}
