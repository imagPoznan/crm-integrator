﻿using System;
using System.Windows;
using EmptyProject.Controllers;
using EmptyProject.Extensions;
using EmptyProject.ModelViews;
using EmptyProject.Views;

namespace EmptyProject
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
        }

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            MessageBox.Show("Test");
            try
            {
                if (e.Args.Length > 1)
                {

                    var param = e?.Args[1].Split(',');
                    var rodzajDokumentu = param[1];
                    if (rodzajDokumentu == "202")
                    {
                        GlobalSettingsSingletonController.Instance.IdZam = Convert.ToInt32(param[0]);
                        GlobalSettingsSingletonController.Instance.StartAppType = StartAppType.WfMag;

                        var mainWindow = new MainWindow();
                        mainWindow.Show();
                    }
              
                    if (rodzajDokumentu == "1")
                    {
                        GlobalSettingsSingletonController.Instance.IdDokWz = Convert.ToInt32(param[0]);
                        GlobalSettingsSingletonController.Instance.StartAppType = StartAppType.WfMag;
                        var mainWindow = new MainWindow();
                        mainWindow.Show();
                    }
                    if (rodzajDokumentu == "4")
                    {
                        GlobalSettingsSingletonController.Instance.IdDokHan = Convert.ToInt32(param[0]);
                        GlobalSettingsSingletonController.Instance.StartAppType = StartAppType.WfMag;
                        var mainWindow = new MainWindow();
                        mainWindow.Show();
                    }


                }
                else
                {
#if DEBUG
                    GlobalSettingsSingletonController.Instance.IdDokWz = 40026;
                    GlobalSettingsSingletonController.Instance.StartAppType = StartAppType.WfMag;
#endif
                    var mainWindow = new MainWindow();
                    mainWindow.Show();
                }
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                Environment.Exit(1);
            }
         
        }
        public enum RodzajDokumentu
        {
            Zamowienie = 202,
            DokumentMagazynowy = 1,
            DokumentHandlowy = 4
        }
    }
}