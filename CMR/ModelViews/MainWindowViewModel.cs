using System;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using CrystalDecisions.CrystalReports.Engine;
using EmptyProject.Controllers;
using EmptyProject.Extensions;
using EmptyProject.Models.Models;
using EmptyProject.Views;
using MaterialDesignThemes.Wpf;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using MessageBox = System.Windows.Forms.MessageBox;

namespace EmptyProject.ModelViews
{
    public class MainWindowViewModel : BindableBaseImpl
    {
        private DataBaseController _dataBaseConnection;

        private DaneOdbiorcyModel _daneOdbiorcyModel;

        public GlobalSettingsSingletonController GlobalSettingsSingletonController
            => GlobalSettingsSingletonController.Instance;

        public static DataModel DataModel { get; set; } = new DataModel();

        public DaneOdbiorcyModel DaneOdbiorcyModel
        {
            get { return _daneOdbiorcyModel; }
            set
            {
                _daneOdbiorcyModel = value;
                OnPropertyChanged();
            }
        }


        public DataBaseController DataBaseConnection
        {
            get { return _dataBaseConnection; }
            set
            {
                _dataBaseConnection = value;
                OnPropertyChanged();
            }
        }


        public decimal VersionApp => GlobalSettingsSingletonController.VersionApp;

        public ICommand SaveDatabaseData { get; set; }
        public ICommand CreateTask { get; set; }
        public ICommand RemoveTask { get; set; }
        public ICommand ConnectDatabase { get; set; }
        public ICommand CheckLicenseButton { get; set; }
        public ICommand Loaded { get; set; }
        public ICommand GetDokumentMagazynowyById { get; set; }
        public ICommand GetZamowienieById { get; set; }
        public ICommand GetDokumentHandlowyById { get; set; }
        public ICommand CreateOperationWfMag { get; set; }
        public ICommand Generuj { get; set; }
        public ICommand PodgladRaportCrystal { get; set; }
        public ICommand OdswiezDokument { get; set; }
        public ICommand WybierzSciezke { get; set; }
  
       


        public MainWindowViewModel()
        {
            InitObjects();
            InitBindings();

        }


        private void InitObjects()
        {
            _dataBaseConnection = DataBaseController.Instance;
            _daneOdbiorcyModel = new DaneOdbiorcyModel();   
        }

        private void InitBindings()
        {
            SaveDatabaseData = new LoaderAwaitableDelegateCommand(SaveDatabaseDataAction);
            ConnectDatabase = new LoaderAwaitableDelegateCommand(ConnectDatabaseAction);
            CheckLicenseButton = new LoaderAwaitableDelegateCommand(CheckLicenseButtonAction);
            Loaded = new LoaderAwaitableDelegateCommand(LoadedAction);
            GetDokumentMagazynowyById = new LoaderAwaitableDelegateCommand(GetDokumentMagazynowyByIdAction);
            GetDokumentHandlowyById = new LoaderAwaitableDelegateCommand(GetDokumentHandlowyByIdAction);
            CreateOperationWfMag = new LoaderAwaitableDelegateCommand(CreateOperationWfMagAction);
            OdswiezDokument = new AwaitableDelegateCommand(OdswiezDokumentAction);
            GetZamowienieById = new LoaderAwaitableDelegateCommand(GetZamowienieByIdAction);
            Generuj = new LoaderAwaitableDelegateCommand(GenerujAction);
       
            WybierzSciezke = new AwaitableDelegateCommand(WybierzSciezkeAction);
        }

       

        private  async Task WybierzSciezkeAction()
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            GlobalSettingsSingletonController.Instance.SciezkaZapisuEtykiety = dialog.ShowDialog() == DialogResult.OK ? dialog.SelectedPath : string.Empty;
        }

        private async Task OdswiezDokumentAction()
        {
            await Task.Factory.StartNew(
                () =>
                {
                    DaneOdbiorcyModel = DokumentMagazynowyController.LoadedActionAsync(DaneOdbiorcyModel);
                    
                });
        }

        private async Task CreateOperationWfMagAction()
        {
            var resp = false;
          
            await Task.Factory.StartNew(() => { resp = ActionsController.CreateOperationWfMagActionAsync(); });
          
            await 
           
                ActionsController.ShowMessagebox(resp
                    ? @"Zadanie zostało dodane do operacji dodatkowych pod nazwą Moduł CMR IMAG"
                    : @"Wystąpił błąd");
        }

        private async Task GenerujAction()
        {
           //TODO Generowanie etykiety
            await Task.Factory.StartNew(() =>
            {
                //ZamowienieController.ZapiszNumerListu(GlobalSettingsSingletonController.IdZam,DaneOdbiorcyModel.NumerPrzesylki);

                PrintingController.PrintPdf(GlobalSettingsSingletonController.Instance.DrukarkaRaportow, "Letter",
                   @"C:\DPD Polska\DPD Polska - Moduł Wysyłkowy\Etykiety\DPD_00000010000140_label_National.pdf", 1);

            });
                     
        }


        private async Task GetDokumentMagazynowyByIdAction()
        {
            await Task.Factory.StartNew(
                () => { DokumentMagazynowyController.GetDokumentMagazynowyByIdAsync(DaneOdbiorcyModel); });
        }
        private async Task GetDokumentHandlowyByIdAction()
        {
            await Task.Factory.StartNew(
                () => { DokumentHandlowyController.GetDokHandlowyByIdAsync(DaneOdbiorcyModel); });
        }

        private async Task GetZamowienieByIdAction()
        {
            await Task.Factory.StartNew(
                () => { ZamowienieController.GetZamowienieByIdAsync(DaneOdbiorcyModel); });
        }

        private async Task LoadedAction()
        {
            await Task.Factory.StartNew(() =>
            {
                ActionsController.SetLicenseInit();
                if (GlobalSettingsSingletonController.Instance.IdZam != 0)
                {
                    DaneOdbiorcyModel = ZamowienieController.LoadedActionAsync(DaneOdbiorcyModel);
                    if (string.Equals(DaneOdbiorcyModel.FormaPlatnosci,"pobranie",StringComparison.CurrentCultureIgnoreCase))
                        DaneOdbiorcyModel.Pobranie = true;
                }
              
            
                if (GlobalSettingsSingletonController.Instance.IdDokWz != 0)
                     DaneOdbiorcyModel = DokumentMagazynowyController.LoadedActionAsync(DaneOdbiorcyModel);
                if (GlobalSettingsSingletonController.Instance.IdDokHan != 0)
                    DaneOdbiorcyModel = DokumentHandlowyController.LoadedActionAsync(DaneOdbiorcyModel);


            });
        }

        private static async Task CheckLicenseButtonAction()
        {
            var resp = String.Empty;
            await Task.Factory.StartNew(() => { resp = LicenseManagerController.CheckLicenseButtonAction(); });
            await ActionsController.ShowMessagebox(GlobalSettingsSingletonController.Instance.IsLicense
                ? $@"{resp}{Environment.NewLine}{GlobalSettingsSingletonController.Instance.DataWygasnieciaLicencji}"
                : $"Brak licencji: {resp}");
        }


        private async Task SaveDatabaseDataAction()
        {
            var resp = false;
            await Task.Factory.StartNew(() => { resp = DataBaseController.SaveDatabaseDataAction(DataBaseConnection); });
            await ActionsController.ShowMessagebox(resp ? @"Baza danych została zapisana" : @"Wystąpił błąd");
        }


        private async Task ConnectDatabaseAction()
        {
            var resp = false;
            await Task.Factory.StartNew(() => { resp = DataBaseController.ConnectDatabase(); });
            await ActionsController.ShowMessagebox(resp ? @"Połączenie zostało nawiązane" : @"Wystąpił błąd");
        }

        public static DialogClosingEventHandler ClosingEventHandler { get; set; }

        public static void OpenTask(object taskContent)
        {
            GlobalSettingsSingletonController.Instance.TaskContent = taskContent;
            GlobalSettingsSingletonController.Instance.IsTaskOpen = true;
        }

        public static void ClosingTask()
        {
            GlobalSettingsSingletonController.Instance.IsTaskOpen = false;
        }
    }

    public enum StartAppType
    {
        Normal,
        WfMag,
        Hidden
    }
}