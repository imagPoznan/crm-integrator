﻿using System;
using System.Collections.Generic;

namespace EmptyProject.Models.Models
{
    public class DaneOdbiorcyModel
    {
        private DateTime _dataZaladowania = DateTime.Now;

        public decimal IdDokumentuMagazynowego { get; set; }
        public decimal IdZamowienia { get; set; }
        public decimal IdKontrahenta { get; set; }
        public decimal WartoscBrutto { get; set; }
        public string NumerDokumentu { get; set; }
        public AdresModel AdresNadawcy { get; set; }
        public AdresModel AdresOdbiorcy { get; set; }
        public AdresModel AdresPrzewoznika { get; set; }
        public AdresModel AdresKolejnegoPrzewoznika { get; set; }
        public string MiejscePrzeznaczenia { get; set; }
        public string MiejsceZaladowania { get; set; }
        public string NumerPrzesylki { get; set; }
        public bool Pobranie { get; set; }
        public string FormaPlatnosci { get; set; }
        
       
public DateTime DataZaladowania
        {
            get { return _dataZaladowania; }
            set { _dataZaladowania = value.Date; }
        } 
        public List<DokumentHandlowyModel> ZalaczoneDokumenty { get; set; }
        public List<ListaTowarowModel> ListaTowarow { get; set; }
        public string Klasa { get; set; }
        public string NrUn { get; set; }
        public string Gp { get; set; }
        public string Adr { get; set; }
        public DateTime WystawionoDnia { get; set; } = DateTime.Now;
        public string WystawionoW { get; set; }
        public string Zaplata { get; set; }
        public string PodpisPrzewoznika { get; set; }
        public string PodpisNadawcy { get; set; }
        public string PostanowieniaSpecjalne { get; set; }
        public string InstrukcjeNadawcy { get; set; }
        public string PostanowieniaOdnosniePrzewoznego { get; set; }
        public string ZastrzezeniaUwagiPrzewoznika { get; set; }
       
   
           
    }
}
