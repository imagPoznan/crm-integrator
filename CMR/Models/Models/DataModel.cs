﻿using System.Collections.Generic;
using EmptyProject.Controllers;
using EmptyProject.Extensions;

namespace EmptyProject.Models.Models
{
    public class DataModel : BindableBaseImpl
    {

        public Dictionary<decimal, string> DaneNadawcy { get; set; } = FirmaController.GetAllAdresyFirmyAdd0();

        public Dictionary<FieldsFromEnum, string> OdbiorcaSelectList { get; set; } = new Dictionary<FieldsFromEnum, string>
        {
            {FieldsFromEnum.None, "Nie pobieraj"},
            {FieldsFromEnum.WzMiejsceDostawy, "WZ - Adres Dostawy"},
            {FieldsFromEnum.Kontrahent, "Kontrahent - Karta Kontrahenta"},
            {FieldsFromEnum.MiejsceDostawyZamowienie,"Zamówienie - Miejsce Dostawy"}
        };

        public Dictionary<FieldsFromEnum, string> MiejsceZaladowaniaSelectList { get; set; } = new Dictionary<FieldsFromEnum, string>
        {
            {FieldsFromEnum.None, "Nie pobieraj"},
            {FieldsFromEnum.FvMiejsceWystawienia, "FV - Miejsce wystawienia"},
        };

        public Dictionary<FieldsFromEnum, string> DataZaladowaniaSelectList { get; set; } = new Dictionary<FieldsFromEnum, string>
        {
            {FieldsFromEnum.None, "Nie pobieraj"},
            {FieldsFromEnum.FvDataWystawieniaDokumentu, "FV - Data wystawienia"},
            {FieldsFromEnum.WzDataWystawienia, "WZ - Data wystawienia"},
        };

        public Dictionary<FieldsFromEnum, string> PrzewoznikSelectList { get; set; } = new Dictionary<FieldsFromEnum, string>
        {
            {FieldsFromEnum.None, "Nie pobieraj"},
            {FieldsFromEnum.WzMiejsceDostawy, "WZ - Adres Dostawy"},
            {FieldsFromEnum.Kontrahent, "Kontrahent - Karta Kontrahenta"}
        };

        public Dictionary<FieldsFromEnum, string> NrRejPrzewoznikSelectList { get; set; } = new Dictionary<FieldsFromEnum, string>
        {
            {FieldsFromEnum.None, "Nie pobieraj"},
            {FieldsFromEnum.WzPole1, "WZ - Pole 1"},
            {FieldsFromEnum.WzPole2, "WZ - Pole 2"},
            {FieldsFromEnum.WzPole3, "WZ - Pole 3"},
            {FieldsFromEnum.WzPole4, "WZ - Pole 4"},
            {FieldsFromEnum.WzPole5, "WZ - Pole 5"},
            {FieldsFromEnum.WzPole6, "WZ - Pole 6"},
            {FieldsFromEnum.WzPole7, "WZ - Pole 7"},
            {FieldsFromEnum.WzPole8, "WZ - Pole 8"},
            {FieldsFromEnum.WzPole9, "WZ - Pole 9"},
            {FieldsFromEnum.WzPole10, "WZ - Pole 10"},
        };
        

        public Dictionary<FieldsFromEnum, string> TowarSelectList { get; set; } = new Dictionary<FieldsFromEnum, string>
        {
            {FieldsFromEnum.None, "Nie pobieraj"},
            {FieldsFromEnum.ArtykulNazwa, "Artukul - Nazwa"},
            {FieldsFromEnum.ArtukulNazwaOrg, "Artukul - Nazwa oryginalna"},
            {FieldsFromEnum.ArtukulNazwaNazwaOrg, "Artukul - Nazwa + Nazwa oryginalna"},
            {FieldsFromEnum.ArtukulNazwaNazwaCd, "Artukul - Nazwa + Nazwa cd"},
            {FieldsFromEnum.ArtukulNazwaNazwaCdNazwaOrg, "Artukul - Nazwa + Nazwa cd + Nazwa oryginalna"}
        };

        public Dictionary<FieldsFromEnum, string> OpakowanieSelectList { get; set; } = new Dictionary
            <FieldsFromEnum, string>
            {
                {FieldsFromEnum.None, "Nie pobieraj"},
            };

        public Dictionary<FieldsFromEnum, string> RodzajTowarSelectList { get; set; } = new Dictionary
            <FieldsFromEnum, string>
            {
                {FieldsFromEnum.None, "Nie pobieraj"},
            };

        public Dictionary<FieldsFromEnum, string> NrStatystycznySelectList { get; set; } = new Dictionary
            <FieldsFromEnum, string>
            {
                {FieldsFromEnum.None, "Nie pobieraj"},
            };

        public Dictionary<FieldsFromEnum, string> InstrukcjeNadawcySelectList { get; set; } = new Dictionary
            <FieldsFromEnum, string>
            {
                {FieldsFromEnum.None, "Nie pobieraj"},
            };

        public Dictionary<FieldsFromEnum, string> PostanowieniePrzewoznySelectList { get; set; } = new Dictionary
            <FieldsFromEnum, string>
            {
                {FieldsFromEnum.None, "Nie pobieraj"},
            };

        public Dictionary<FieldsFromEnum, string> ZaplataSelectList { get; set; } = new Dictionary
            <FieldsFromEnum, string>
            {
                {FieldsFromEnum.None, "Nie pobieraj"},
            };

        public Dictionary<FieldsFromEnum, string> ZastrzezeniaIUwagiPrzewoznikaSelectList { get; set; } = new Dictionary
            <FieldsFromEnum, string>
            {
                {FieldsFromEnum.None, "Nie pobieraj"},
            };

        public Dictionary<FieldsFromEnum, string> PostanowieniaSpecjalneSelectList { get; set; } = new Dictionary
            <FieldsFromEnum, string>
            {
                {FieldsFromEnum.None, "Nie pobieraj"},
            };

        public Dictionary<FieldsFromEnum, string> WystawionoWSelectList { get; set; } = new Dictionary
            <FieldsFromEnum, string>
            {
                {FieldsFromEnum.None, "Nie pobieraj"},
            };

        public Dictionary<FieldsFromEnum, string> PodpisNadawcySelectList { get; set; } = new Dictionary
            <FieldsFromEnum, string>
            {
                {FieldsFromEnum.None, "Nie pobieraj"},
            };

        public Dictionary<FieldsFromEnum, string> PodpisPrzewoznikaSelectList { get; set; } = new Dictionary
            <FieldsFromEnum, string>
            {
                {FieldsFromEnum.None, "Nie pobieraj"},
            };
    }
   

    public enum FieldsFromEnum
    {
        None,
        WzMiejsceDostawy,
        WzDataWystawienia,
        Kontrahent,
        Fv,
        FvMiejsceWystawienia,
        FvDataWystawieniaDokumentu,
        ArtykulNazwa,
        ArtukulNazwaOrg,
        ArtukulNazwaNazwaOrg,
        ArtukulNazwaNazwaCd,
        ArtukulNazwaNazwaCdNazwaOrg,
        WzPole1,
        WzPole2,
        WzPole3,
        WzPole4,
        WzPole5,
        WzPole6,
        WzPole7,
        WzPole8,
        WzPole9,
        WzPole10,
        MiejsceDostawyZamowienie
    }



}
