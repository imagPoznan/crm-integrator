﻿namespace EmptyProject.Models.Models
{
    public class QueryModel
    {

        private static string CreateStringAlterTable(string tableName, string field, string properties)
        {
            return
                $@" --dodajemy {field}
                           IF NOT EXISTS (
                             SELECT *
                             FROM   sys.columns
                             WHERE  object_id = OBJECT_ID(N'{tableName}') AND name = '{field}'
                            )
                            BEGIN
                                    ALTER TABLE {tableName}
                                    ADD {field} {properties}
                            END ";
        }

        public static string Sprawdz()
        {
            var query = @"Select * from zamowienie";
            return query;
        }
        public static string CreateCmrHistory()
        {
            var query = @"IF (not EXISTS (SELECT * 
                FROM INFORMATION_SCHEMA.TABLES 
                WHERE TABLE_SCHEMA = 'dbo' 
                AND  TABLE_NAME = '_IMAG_CMR'))
                BEGIN
                    create table _IMAG_CMR(
                        id int IDENTITY(1,1) PRIMARY KEY NOT NULL,
                        id_dokumentu_magazynowego int NOT NULL,

                        numer_dokumentu_magazynowego varchar(500) NULL,

                        nadawca_firma varchar(500) NULL,
                        nadawca_imie_nazwisko varchar(500) NULL,
                        nadawca_ulica varchar(500) NULL,
                        nadawca_kod_pocztowy varchar(500) NULL,
                        nadawca_miejscowosc varchar(500) NULL,
                        nadawca_kraj varchar(500) NULL,

                        odbiorca_firma varchar(500) NULL,
                        odbiorca_imie_nazwisko varchar(500) NULL,
                        odbiorca_ulica varchar(500) NULL,
                        odbiorca_kod_pocztowy varchar(500) NULL,
                        odbiorca_miejscowosc varchar(500) NULL,
                        odbiorca_kraj varchar(500) NULL,

                        miejsce_przeznaczenia varchar(500) NULL,
                        miejsce_zaladowania varchar(500) NULL,
                        data_zaladowania datetime NULL,
                        zalaczone_dokumenty varchar(500) NULL,

                        towar_1_cechy_i_numery varchar(500) NULL,
                        towar_1_ilosc_sztuk int NOT NULL default(0),
                        towar_1_sposob_opakowania varchar(500) NULL,
                        towar_1_rodzaj_towaru varchar(500) NULL,
                        towar_1_numer_statystyczny varchar(500) NULL,
                        towar_1_waga_brutto_w_kg varchar(500) NULL,
                        towar_1_objetosc_w_m3 varchar(500) NULL,

                        towar_2_cechy_i_numery varchar(500) NULL,
                        towar_2_ilosc_sztuk int NOT NULL default(0),
                        towar_2_sposob_opakowania varchar(500) NULL,
                        towar_2_rodzaj_towaru varchar(500) NULL,
                        towar_2_numer_statystyczny varchar(500) NULL,
                        towar_2_waga_brutto_w_kg varchar(500) NULL,
                        towar_2_objetosc_w_m3 varchar(500) NULL,

                        towar_3_cechy_i_numery varchar(500) NULL,
                        towar_3_ilosc_sztuk int NOT NULL default(0),
                        towar_3_sposob_opakowania varchar(500) NULL,
                        towar_3_rodzaj_towaru varchar(500) NULL,
                        towar_3_numer_statystyczny varchar(500) NULL,
                        towar_3_waga_brutto_w_kg varchar(500) NULL,
                        towar_3_objetosc_w_m3 varchar(500) NULL,

                        towar_4_cechy_i_numery varchar(500) NULL,
                        towar_4_ilosc_sztuk int NOT NULL default(0),
                        towar_4_sposob_opakowania varchar(500) NULL,
                        towar_4_rodzaj_towaru varchar(500) NULL,
                        towar_4_numer_statystyczny varchar(500) NULL,
                        towar_4_waga_brutto_w_kg varchar(500) NULL,
                        towar_4_objetosc_w_m3 varchar(500) NULL,

                        klasa varchar(500) NULL,
                        nr_un varchar(500) NULL,
                        gp varchar(500) NULL,
                        adr varchar(500) NULL,
                        instrukcje_nadawcy varchar(500) NULL,
                        postanowienia_odnosnie_przewoznego varchar(500) NULL,

                        przewoznik_firma varchar(500) NULL,
                        przewoznik_imie_nazwisko varchar(500) NULL,
                        przewoznik_ulica varchar(500) NULL,
                        przewoznik_kod_pocztowy varchar(500) NULL,
                        przewoznik_miejscowosc varchar(500) NULL,
                        przewoznik_kraj varchar(500) NULL,

                        przewoznik_kolejny_firma varchar(500) NULL,
                        przewoznik_kolejny_imie_nazwisko varchar(500) NULL,
                        przewoznik_kolejny_ulica varchar(500) NULL,
                        przewoznik_kolejny_kod_pocztowy varchar(500) NULL,
                        przewoznik_kolejny_miejscowosc varchar(500) NULL,
                        przewoznik_kolejny_kraj varchar(500) NULL,
                        
                        przewoznik_zastrzezenie_i_uwagi varchar(500) NULL,

                        zaplata varchar(500) NULL,
                        podpis_nadawcy varchar(500) NULL,
                        podpis_przewoznika varchar(500) NULL,
                        wystawiono_w varchar(500) NULL,
                        wystawiono_dnia datetime NULL,

                )
                END";
            
            query += CreateStringAlterTable("_IMAG_CMR", "id_dokumentu_magazynowego", "int NOT NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "numer_dokumentu_magazynowego", "varchar(500) NULL");

            query += CreateStringAlterTable("_IMAG_CMR", "nadawca_firma", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "nadawca_imie_nazwisko", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "nadawca_ulica", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "nadawca_kod_pocztowy", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "nadawca_miejscowosc", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "nadawca_kraj", "varchar(500) NULL");

            query += CreateStringAlterTable("_IMAG_CMR", "odbiorc_kolejny_firma", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "odbiorca_imie_nazwisko", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "odbiorca_ulica", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "odbiorca_kod_pocztowy", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "odbiorca_miejscowosc", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "odbiorca_kraj", "varchar(500) NULL");

            query += CreateStringAlterTable("_IMAG_CMR", "miejsce_przeznaczenia", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "miejsce_zaladowania", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "data_zaladowania", "datetime NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "zalaczone_dokumenty", "varchar(500) NULL");

            query += CreateStringAlterTable("_IMAG_CMR", "towar_1_cechy_i_numery", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "towar_1_ilosc_sztuk", "int NOT NULL default(0)");
            query += CreateStringAlterTable("_IMAG_CMR", "towar_1_sposob_opakowania", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "towar_1_rodzaj_towaru", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "towar_1_numer_statystyczny", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "towar_1_waga_brutto_w_kg", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "towar_1_objetosc_w_m3", "varchar(500) NULL");

            query += CreateStringAlterTable("_IMAG_CMR", "towar_2_cechy_i_numery", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "towar_2_ilosc_sztuk", "int NOT NULL default(0)");
            query += CreateStringAlterTable("_IMAG_CMR", "towar_2_sposob_opakowania", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "towar_2_rodzaj_towaru", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "towar_2_numer_statystyczny", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "towar_2_waga_brutto_w_kg", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "towar_2_objetosc_w_m3", "varchar(500) NULL");

            query += CreateStringAlterTable("_IMAG_CMR", "towar_3_cechy_i_numery", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "towar_3_ilosc_sztuk", "int NOT NULL default(0)");
            query += CreateStringAlterTable("_IMAG_CMR", "towar_3_sposob_opakowania", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "towar_3_rodzaj_towaru", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "towar_3_numer_statystyczny", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "towar_3_waga_brutto_w_kg", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "towar_3_objetosc_w_m3", "varchar(500) NULL");

            query += CreateStringAlterTable("_IMAG_CMR", "towar_4_cechy_i_numery", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "towar_4_ilosc_sztuk", "int NOT NULL default(0)");
            query += CreateStringAlterTable("_IMAG_CMR", "towar_4_sposob_opakowania", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "towar_4_rodzaj_towaru", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "towar_4_numer_statystyczny", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "towar_4_waga_brutto_w_kg", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "towar_4_objetosc_w_m3", "varchar(500) NULL");

            query += CreateStringAlterTable("_IMAG_CMR", "klasa", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "nr_un", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "gp", " varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "adr", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "instrukcje_nadawcy", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "postanowienia_odnosnie_przewoznego", "varchar(500) NULL");

            query += CreateStringAlterTable("_IMAG_CMR", "przewoznik_firma", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "przewoznik_imie_nazwisko", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "przewoznik_ulica", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "przewoznik_kod_pocztowy", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "przewoznik_miejscowosc", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "przewoznik_kraj", "varchar(500) NULL");

            query += CreateStringAlterTable("_IMAG_CMR", "przewoznik_kolejny_firma", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "przewoznik_kolejny_imie_nazwisko", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "przewoznik_kolejny_ulica", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "przewoznik_kolejny_kod_pocztowy", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "przewoznik_kolejny_miejscowosc", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "przewoznik_kolejny_kraj", "varchar(500) NULL");

            query += CreateStringAlterTable("_IMAG_CMR", "przewoznik_zastrzezenie_i_uwagi", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "zaplata", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "podpis_nadawcy", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "podpis_przewoznika", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "wystawiono_w", "varchar(500) NULL");
            query += CreateStringAlterTable("_IMAG_CMR", "wystawiono_dnia", "datetime NULL");
            
            return query;
        }
    }
}
