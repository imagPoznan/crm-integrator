﻿using System.Data.Entity;
using System.Linq;
using EmptyProject.Controllers;
using EmptyProject.Models.Models;
using System.Windows;

namespace EmptyProject.Models.Repository
{
    class DokumentHandlowyRepository : Repository<DOKUMENT_HANDLOWY>
    {
        public DatabaseClientEntities context;
        public DbSet<DatabaseClientEntities> dbSet;

        public DokumentHandlowyRepository(DatabaseClientEntities context) : base(context)
        {
            this.context = context;
            dbSet = context.Set<DatabaseClientEntities>();
        }
        public DaneOdbiorcyModel GetComplexById(decimal id)
        {
            var handlowy = Context.DOKUMENT_HANDLOWY.FirstOrDefault(x => x.ID_DOKUMENTU_HANDLOWEGO == id);

            if (handlowy == null)
                return new DaneOdbiorcyModel();
            var complex = new DaneOdbiorcyModel
            {
                IdZamowienia = handlowy.ID_DOKUMENTU_HANDLOWEGO,
                IdKontrahenta = handlowy.ID_KONTRAHENTA ?? 0,
                WartoscBrutto = handlowy.WARTOSC_BRUTTO ?? 0,
                AdresOdbiorcy = MiejsceDostawyController.GetAdresByIdDokMag(handlowy.ID_DOKUMENTU_HANDLOWEGO, handlowy.ID_KONTRAHENTA ?? 0),
                NumerDokumentu = handlowy.NUMER,

            };

            return complex;
        }

        public string GetMiejsceZaladowaniaByIdDok(decimal idFv)
        {
            return (from fv in Context.DOKUMENT_HANDLOWY
                where fv.ID_DOKUMENTU_HANDLOWEGO == idFv
                select fv.MIEJSCE
            ).FirstOrDefault();
        }

        public DOKUMENT_HANDLOWY GetByIdWz(decimal idWz)
        {
            return (from wz in Context.DOKUMENT_MAGAZYNOWY
                join handlowy in Context.DOKUMENT_HANDLOWY on wz.ID_DOKUMENTU_HANDLOWEGO equals
                handlowy.ID_DOKUMENTU_HANDLOWEGO
                where wz.ID_DOK_MAGAZYNOWEGO == idWz
                select handlowy
            ).FirstOrDefault();
        }
    }
}