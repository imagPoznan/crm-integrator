﻿using System;
using System.Data.Entity;
using System.Linq;
using EmptyProject.Controllers;
using EmptyProject.Models.Models;
using System.Windows;

namespace EmptyProject.Models.Repository
{
    class ZamowienieRepository : Repository<ZAMOWIENIE>
    {
        public DatabaseClientEntities context;
        public DbSet<DatabaseClientEntities> dbSet;

        public ZamowienieRepository(DatabaseClientEntities context) : base(context)
        {
            this.context = context;
            dbSet = context.Set<DatabaseClientEntities>();
        }

        public DaneOdbiorcyModel GetComplexById(decimal id)
        {

            var zamowienie = Context.ZAMOWIENIEs.FirstOrDefault(x => x.ID_ZAMOWIENIA == id);

            if (zamowienie == null)
                return new DaneOdbiorcyModel();
            var complex = new DaneOdbiorcyModel
            {
                IdZamowienia = zamowienie.ID_ZAMOWIENIA,
                IdKontrahenta = zamowienie.ID_KONTRAHENTA ?? 0,
                WartoscBrutto = zamowienie.WARTOSC_BRUTTO ?? 0,
                AdresOdbiorcy =
                    MiejsceDostawyController.GetAdresByIdDokMag(zamowienie.ID_ZAMOWIENIA, zamowienie.ID_KONTRAHENTA ?? 0),
                NumerDokumentu = zamowienie.NUMER,
               FormaPlatnosci = zamowienie.FORMA_PLATNOSCI

            };

            return complex;
        }

       

    }
    }

