﻿using System.Data.Entity;
using System.Linq;
using EmptyProject.Models.Models;

namespace EmptyProject.Models.Repository
{
    class MiejsceDostawyRepository : Repository<MIEJSCE_DOSTAWY>
    {
        public DatabaseClientEntities context;
        public DbSet<DatabaseClientEntities> dbSet;

        public MiejsceDostawyRepository(DatabaseClientEntities context) : base(context)
        {
            this.context = context;
            dbSet = context.Set<DatabaseClientEntities>();
        }

        public AdresModel GetAdresByIdDokMag(decimal idDokMag)
        {
            return (from dostawa in Context.DOSTAWAs
                    join miejsceDostawy in Context.MIEJSCE_DOSTAWY on dostawa.ID_MIEJSCA_DOSTAWY equals
                    miejsceDostawy.ID_MIEJSCA_DOSTAWY
                    where dostawa.ID_DOK_MAGAZYNOWEGO.Value == idDokMag
                    select new AdresModel
                    {
                        Nazwa = miejsceDostawy.FIRMA,
                        ImieINazwisko = miejsceDostawy.ODBIORCA,
                        Kraj = miejsceDostawy.SYM_KRAJU,
                        KodPocztowy = miejsceDostawy.KOD_POCZTOWY,
                        Ulica = miejsceDostawy.ULICA_LOKAL,
                        Miejscowosc = miejsceDostawy.MIEJSCOWOSC,
                        Telefon = miejsceDostawy.TEL
                       
                    }).FirstOrDefault();
        }

        public AdresModel GetAdresByIdZam(decimal idZam)
        {
            return (from dostawa in Context.DOSTAWAs
                join miejsceDostawy in Context.MIEJSCE_DOSTAWY on dostawa.ID_MIEJSCA_DOSTAWY equals
                miejsceDostawy.ID_MIEJSCA_DOSTAWY
                where dostawa.ID_ZAMOWIENIA.Value == idZam
                select new AdresModel
                {
                    Nazwa = miejsceDostawy.FIRMA,
                    ImieINazwisko = miejsceDostawy.ODBIORCA,
                    Kraj = miejsceDostawy.SYM_KRAJU,
                    KodPocztowy = miejsceDostawy.KOD_POCZTOWY,
                    Ulica = miejsceDostawy.ULICA_LOKAL,
                    Miejscowosc = miejsceDostawy.MIEJSCOWOSC,
                    Telefon = miejsceDostawy.TEL
                }

            ).FirstOrDefault();
        }


        public string GetMiejscePrzeznaczeniaByIdDok(decimal idDokMag)
        {
            return (from dostawa in Context.DOSTAWAs
                    join miejsceDostawy in Context.MIEJSCE_DOSTAWY on dostawa.ID_MIEJSCA_DOSTAWY equals miejsceDostawy.ID_MIEJSCA_DOSTAWY
                    where dostawa.ID_DOK_MAGAZYNOWEGO.Value == idDokMag
                    select miejsceDostawy.MIEJSCOWOSC
                    ).FirstOrDefault();
        }
    }
}
