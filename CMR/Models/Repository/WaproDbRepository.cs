﻿using System.Data.Entity;
using System.Linq;

namespace EmptyProject.Models.Repository
{
    internal class WaproDbRepository : Repository<WAPRODB>
    {
        public DatabaseClientEntities context;
        public DbSet<DatabaseClientEntities> dbSet;

        public WaproDbRepository(DatabaseClientEntities context) : base(context)
        {
            this.context = context;
            dbSet = context.Set<DatabaseClientEntities>();
        }

        public string GetDbId()
        {
            return Context.WAPRODBs.FirstOrDefault()?.WAPRODB_ID.ToString();
        }
    }
}
