﻿using System.Collections.Generic;

namespace EmptyProject.Models.Repository
{
    public interface IRepository<out TEntity> where TEntity : class
    {
        IEnumerable<TEntity> Get();

        TEntity GetById(object id);

    }
}
