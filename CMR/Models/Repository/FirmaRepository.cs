﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using EmptyProject.Models.Models;

namespace EmptyProject.Models.Repository
{
    class FirmaRepository : Repository<FIRMA>
    {
        public DatabaseClientEntities context;
        public DbSet<DatabaseClientEntities> dbSet;

        public FirmaRepository(DatabaseClientEntities context) : base(context)
        {
            this.context = context;
            dbSet = context.Set<DatabaseClientEntities>();
        }

        public AdresModel GetAdresFirmyByIdFirma(decimal idAdresFirma)
        {
             return(from firma in Context.FIRMAs
                    join adresyFirmy in Context.ADRESY_FIRMY on firma.ID_FIRMY equals adresyFirmy.ID_FIRMY
                    where adresyFirmy.ID_ADRESY_FIRMY == idAdresFirma
                    select new AdresModel
                    {
                        Nazwa = firma.NAZWA_PELNA,
                        Kraj = adresyFirmy.SYM_KRAJU,
                        KodPocztowy = adresyFirmy.KOD_POCZTOWY,
                        Ulica = adresyFirmy.ULICA + " " +adresyFirmy.NR_DOMU + "/"+ adresyFirmy.NR_LOKALU,
                        Miejscowosc = adresyFirmy.MIEJSCOWOSC,
                        Telefon = adresyFirmy.TELEFON,
                        Email = adresyFirmy.E_MAIL,
                    }).FirstOrDefault();
        }

        public Dictionary<decimal, string> GetAllAdresyFirmy()
        {
             var listFirmy=(from firma in Context.FIRMAs
                    join adresyFirmy in Context.ADRESY_FIRMY on firma.ID_FIRMY equals adresyFirmy.ID_FIRMY
                    select new AdresModel
                    {
                        IdAdresuFirmy = adresyFirmy.ID_ADRESY_FIRMY,
                        IdFirmy = adresyFirmy.ID_FIRMY ?? 0,
                        Nazwa = firma.NAZWA_PELNA,
                        Kraj = adresyFirmy.SYM_KRAJU,
                        KodPocztowy = adresyFirmy.KOD_POCZTOWY,
                        Ulica = adresyFirmy.ULICA + " " + adresyFirmy.NR_DOMU + "/" + adresyFirmy.NR_LOKALU,
                        Miejscowosc = adresyFirmy.MIEJSCOWOSC,
                        Telefon = adresyFirmy.TELEFON,
                        Email = adresyFirmy.E_MAIL,
                    }).ToList();

            var dict = new Dictionary<decimal, string>();
            foreach (var row in listFirmy)
            {
                var stringB = new StringBuilder();
                stringB.Append(row.IdFirmy + " ");
                stringB.Append(row.Nazwa + " ");
                stringB.Append(row.Ulica + " ");
                stringB.Append(row.KodPocztowy + " ");
                stringB.Append(row.Miejscowosc + " ");
                stringB.Append(row.Kraj + " ");
                stringB.Append(row.Ulica + " ");
                stringB.Append(row.Telefon + " ");
                stringB.Append(row.Email + " ");
                dict.Add(row.IdAdresuFirmy, stringB.ToString());
            }
            return dict;
        }
    }
}
