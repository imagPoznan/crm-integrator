﻿using System;
using System.Data.Entity;
using System.Linq;
using EmptyProject.Models.Models;

namespace EmptyProject.Models.Repository
{
    class ImagCmrRepository : Repository<C_IMAG_CMR>
    {
        public DatabaseClientEntities context;
        public DbSet<DatabaseClientEntities> dbSet;

        public ImagCmrRepository(DatabaseClientEntities context) : base(context)
        {
            this.context = context;
            dbSet = context.Set<DatabaseClientEntities>();
        }

        public bool Save(DaneOdbiorcyModel dokumentMagazynowyModel)
        {
            var current =context.C_IMAG_CMR.FirstOrDefault(
                    x => x.id_dokumentu_magazynowego == dokumentMagazynowyModel.IdDokumentuMagazynowego);

            if (current != null)
            {
                current.adr = dokumentMagazynowyModel.Adr;
                current.gp = dokumentMagazynowyModel.Gp;
                current.data_zaladowania = dokumentMagazynowyModel.DataZaladowania;
                current.klasa = dokumentMagazynowyModel.Klasa;
                current.instrukcje_nadawcy = dokumentMagazynowyModel.InstrukcjeNadawcy;
                current.miejsce_przeznaczenia = dokumentMagazynowyModel.MiejscePrzeznaczenia;
                current.miejsce_zaladowania = dokumentMagazynowyModel.MiejsceZaladowania;
                current.nr_un = dokumentMagazynowyModel.NrUn;
                current.podpis_nadawcy = dokumentMagazynowyModel.PodpisNadawcy;
                current.podpis_przewoznika = dokumentMagazynowyModel.PodpisPrzewoznika;
                current.postanowienia_odnosnie_przewoznego = dokumentMagazynowyModel.PostanowieniaOdnosniePrzewoznego;
               
                current.data_zaladowania = dokumentMagazynowyModel.DataZaladowania;
                current.wystawiono_dnia = dokumentMagazynowyModel.WystawionoDnia;
                current.wystawiono_w = dokumentMagazynowyModel.WystawionoW;
                current.zaplata = dokumentMagazynowyModel.Zaplata;
                current.przewoznik_zastrzezenie_i_uwagi = dokumentMagazynowyModel.ZastrzezeniaUwagiPrzewoznika;

                current.nadawca_firma = dokumentMagazynowyModel.AdresNadawcy?.Nazwa;
                current.nadawca_imie_nazwisko= dokumentMagazynowyModel.AdresNadawcy?.ImieINazwisko;
                current.nadawca_kod_pocztowy = dokumentMagazynowyModel.AdresNadawcy?.KodPocztowy;
                current.nadawca_ulica = dokumentMagazynowyModel.AdresNadawcy?.Ulica;
                current.nadawca_kraj = dokumentMagazynowyModel.AdresNadawcy?.Kraj;
                current.nadawca_miejscowosc = dokumentMagazynowyModel.AdresNadawcy?.Miejscowosc;

                current.odbiorca_firma = dokumentMagazynowyModel.AdresOdbiorcy?.Nazwa;
                current.odbiorca_imie_nazwisko = dokumentMagazynowyModel.AdresOdbiorcy?.ImieINazwisko;
                current.odbiorca_kod_pocztowy = dokumentMagazynowyModel.AdresOdbiorcy?.KodPocztowy;
                current.odbiorca_ulica = dokumentMagazynowyModel.AdresOdbiorcy?.Ulica;
                current.odbiorca_kraj = dokumentMagazynowyModel.AdresOdbiorcy?.Kraj;
                current.odbiorca_miejscowosc = dokumentMagazynowyModel.AdresOdbiorcy?.Miejscowosc;

                current.przewoznik_firma = dokumentMagazynowyModel.AdresPrzewoznika?.Nazwa;
                current.przewoznik_imie_nazwisko = dokumentMagazynowyModel.AdresPrzewoznika?.ImieINazwisko;
                current.przewoznik_kod_pocztowy = dokumentMagazynowyModel.AdresPrzewoznika?.KodPocztowy;
                current.przewoznik_ulica = dokumentMagazynowyModel.AdresPrzewoznika?.Ulica;
                current.przewoznik_kraj = dokumentMagazynowyModel.AdresPrzewoznika?.Kraj;
                current.przewoznik_miejscowosc = dokumentMagazynowyModel.AdresPrzewoznika?.Miejscowosc;

                current.przewoznik_kolejny_firma = dokumentMagazynowyModel.AdresKolejnegoPrzewoznika?.Nazwa;
                current.przewoznik_kolejny_imie_nazwisko = dokumentMagazynowyModel.AdresKolejnegoPrzewoznika?.ImieINazwisko;
                current.przewoznik_kolejny_kod_pocztowy = dokumentMagazynowyModel.AdresKolejnegoPrzewoznika?.KodPocztowy;
                current.przewoznik_kolejny_ulica = dokumentMagazynowyModel.AdresKolejnegoPrzewoznika?.Ulica;
                current.przewoznik_kolejny_kraj = dokumentMagazynowyModel.AdresKolejnegoPrzewoznika?.Kraj;
                current.przewoznik_kolejny_miejscowosc = dokumentMagazynowyModel.AdresKolejnegoPrzewoznika?.Miejscowosc;
                current.numer_dokumentu_magazynowego = dokumentMagazynowyModel.NumerDokumentu;
                current.zalaczone_dokumenty = string.Join(", ", dokumentMagazynowyModel.ZalaczoneDokumenty);

                Context.SaveChanges();
            }
            else
            {
                var newDok = new C_IMAG_CMR
                {
                    id_dokumentu_magazynowego = Convert.ToInt32(dokumentMagazynowyModel.IdDokumentuMagazynowego),
                    adr = dokumentMagazynowyModel.Adr,
                    gp = dokumentMagazynowyModel.Gp,
                    data_zaladowania = dokumentMagazynowyModel.DataZaladowania,
                    klasa = dokumentMagazynowyModel.Klasa,
                    instrukcje_nadawcy = dokumentMagazynowyModel.InstrukcjeNadawcy,
                    miejsce_przeznaczenia = dokumentMagazynowyModel.MiejscePrzeznaczenia,
                    miejsce_zaladowania = dokumentMagazynowyModel.MiejsceZaladowania,
                    nr_un = dokumentMagazynowyModel.NrUn,
                    podpis_nadawcy = dokumentMagazynowyModel.PodpisNadawcy,
                    podpis_przewoznika = dokumentMagazynowyModel.PodpisPrzewoznika,
                    postanowienia_odnosnie_przewoznego = dokumentMagazynowyModel.PostanowieniaOdnosniePrzewoznego,
                    wystawiono_dnia = dokumentMagazynowyModel.WystawionoDnia,
                    wystawiono_w = dokumentMagazynowyModel.WystawionoW,
                    zaplata = dokumentMagazynowyModel.Zaplata,
                    przewoznik_zastrzezenie_i_uwagi = dokumentMagazynowyModel.ZastrzezeniaUwagiPrzewoznika,

                    nadawca_firma = dokumentMagazynowyModel.AdresNadawcy?.Nazwa,
                    nadawca_imie_nazwisko = dokumentMagazynowyModel.AdresNadawcy?.ImieINazwisko,
                    nadawca_kod_pocztowy = dokumentMagazynowyModel.AdresNadawcy?.KodPocztowy,
                    nadawca_ulica = dokumentMagazynowyModel.AdresNadawcy?.Ulica,
                    nadawca_kraj = dokumentMagazynowyModel.AdresNadawcy?.Kraj,
                    nadawca_miejscowosc = dokumentMagazynowyModel.AdresNadawcy?.Miejscowosc,
                    odbiorca_firma = dokumentMagazynowyModel.AdresOdbiorcy?.Nazwa,
                    odbiorca_imie_nazwisko = dokumentMagazynowyModel.AdresOdbiorcy?.ImieINazwisko,
                    odbiorca_kod_pocztowy = dokumentMagazynowyModel.AdresOdbiorcy?.KodPocztowy,
                    odbiorca_ulica = dokumentMagazynowyModel.AdresOdbiorcy?.Ulica,
                    odbiorca_kraj = dokumentMagazynowyModel.AdresOdbiorcy?.Kraj,
                    odbiorca_miejscowosc = dokumentMagazynowyModel.AdresOdbiorcy?.Miejscowosc,
                    przewoznik_firma = dokumentMagazynowyModel.AdresPrzewoznika?.Nazwa,
                    przewoznik_imie_nazwisko = dokumentMagazynowyModel.AdresPrzewoznika?.ImieINazwisko,
                    przewoznik_kod_pocztowy = dokumentMagazynowyModel.AdresPrzewoznika?.KodPocztowy,
                    przewoznik_ulica = dokumentMagazynowyModel.AdresPrzewoznika?.Ulica,
                    przewoznik_kraj = dokumentMagazynowyModel.AdresPrzewoznika?.Kraj,
                    przewoznik_miejscowosc = dokumentMagazynowyModel.AdresPrzewoznika?.Miejscowosc,

                    przewoznik_kolejny_firma = dokumentMagazynowyModel.AdresKolejnegoPrzewoznika?.Nazwa,
                    przewoznik_kolejny_imie_nazwisko = dokumentMagazynowyModel.AdresKolejnegoPrzewoznika?.ImieINazwisko,
                    przewoznik_kolejny_kod_pocztowy = dokumentMagazynowyModel.AdresKolejnegoPrzewoznika?.KodPocztowy,
                    przewoznik_kolejny_ulica = dokumentMagazynowyModel.AdresKolejnegoPrzewoznika?.Ulica,
                    przewoznik_kolejny_kraj = dokumentMagazynowyModel.AdresKolejnegoPrzewoznika?.Kraj,
                    przewoznik_kolejny_miejscowosc = dokumentMagazynowyModel.AdresKolejnegoPrzewoznika?.Miejscowosc,
                    numer_dokumentu_magazynowego = dokumentMagazynowyModel.NumerDokumentu,
                    zalaczone_dokumenty = string.Join(", ", dokumentMagazynowyModel.ZalaczoneDokumenty),

                };

                Insert(newDok);
            }
            return true;
        }
    }
}
