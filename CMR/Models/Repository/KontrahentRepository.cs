﻿using System.Data.Entity;
using System.Linq;
using EmptyProject.Models.Models;

namespace EmptyProject.Models.Repository
{
    class KontrahentRepository : Repository<KONTRAHENT>
    {
        public DatabaseClientEntities context;
        public DbSet<DatabaseClientEntities> dbSet;

        public KontrahentRepository(DatabaseClientEntities context) : base(context)
        {
            this.context = context;
            dbSet = context.Set<DatabaseClientEntities>();
        }

        public AdresModel GetAdresById(decimal id)
        {
            return Context.KONTRAHENTs.Where(x => x.ID_KONTRAHENTA == id).Select(x => new AdresModel
            {
               Nazwa = x.NAZWA_PELNA,
               Kraj = x.SYM_KRAJU,
               KodPocztowy = x.KOD_POCZTOWY,
               Miejscowosc = x.MIEJSCOWOSC,
               Ulica = x.ULICA_LOKAL,
               KrajEn = x.SYM_KRAJU_KOR,
               Email = x.ADRES_EMAIL,
               Telefon = x.TELEFON_FIRMOWY,
               IdFirmy = x.ID_FIRMY ?? 0
               
            }).FirstOrDefault();
        }
    }
}
