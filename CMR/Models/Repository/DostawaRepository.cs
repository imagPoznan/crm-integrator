﻿using System.Data.Entity;

namespace EmptyProject.Models.Repository
{
    class DostawaRepository : Repository<DOSTAWA>
    {
        public DatabaseClientEntities context;
        public DbSet<DatabaseClientEntities> dbSet;

        public DostawaRepository(DatabaseClientEntities context) : base(context)
        {
            this.context = context;
            dbSet = context.Set<DatabaseClientEntities>();
        }
    }
}
