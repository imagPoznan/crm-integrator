﻿using System.Data.Entity;
using System.Linq;

namespace EmptyProject.Models.Repository
{
    class OperacjeDodatkoweRepository : Repository<MAG_OPERACJE_DODATKOWE>
    {
        public DatabaseClientEntities context;
        public DbSet<DatabaseClientEntities> dbSet;

        public OperacjeDodatkoweRepository(DatabaseClientEntities context) : base(context)
        {
            this.context = context;
            dbSet = context.Set<DatabaseClientEntities>();
        }

        public MAG_OPERACJE_DODATKOWE GetByName(string name)
        {
            return Context.MAG_OPERACJE_DODATKOWE.FirstOrDefault(x=>x.NAZWA == name);
        }

        public bool Add(MAG_OPERACJE_DODATKOWE objectOperacja)
        {
            var operation = Context.MAG_OPERACJE_DODATKOWE.FirstOrDefault(x => x.NAZWA == objectOperacja.NAZWA);
            if (operation != null)
            {
                operation.NAZWA = objectOperacja.NAZWA;
                operation.KOD_GRUPY = objectOperacja.KOD_GRUPY;
                operation.KOD_OPERACJI = objectOperacja.KOD_OPERACJI;
                operation.GRUPOWANIE = objectOperacja.GRUPOWANIE;
                operation.PARAMETRY = objectOperacja.PARAMETRY;
                operation.SCIEZKA = objectOperacja.SCIEZKA;
                operation.SKROT_KLAWISZOWY = objectOperacja.SKROT_KLAWISZOWY;
                operation.SKROT_KLAWISZOWY_KOD = objectOperacja.SKROT_KLAWISZOWY_KOD;
                operation.FUNKCJA_ZEWNETRZNA = objectOperacja.FUNKCJA_ZEWNETRZNA;
                operation.ODSWIEZ_LISTE = objectOperacja.ODSWIEZ_LISTE;
                operation.KASUJ_ZAZNACZENIE = objectOperacja.KASUJ_ZAZNACZENIE;

                Context.SaveChanges();
            }
            else
                Insert(objectOperacja);
            return true;
        }
    }
}
