﻿using System.Data.Entity;

namespace EmptyProject.Models.Repository
{
    internal class ArtykulRepository : Repository<ARTYKUL>
    {
        public DatabaseClientEntities context;
        public DbSet<DatabaseClientEntities> dbSet;

        public ArtykulRepository(DatabaseClientEntities context) : base(context)
        {
            this.context = context;
            dbSet = context.Set<DatabaseClientEntities>();
        }

    }
}
