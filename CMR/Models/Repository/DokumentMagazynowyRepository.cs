﻿using System.Data.Entity;
using System.Linq;
using System.Windows;
using EmptyProject.Controllers;
using EmptyProject.Models.Models;

namespace EmptyProject.Models.Repository
{
    class DokumentMagazynowyRepository : Repository<DOKUMENT_MAGAZYNOWY>
    {
        public DatabaseClientEntities context;
        public DbSet<DatabaseClientEntities> dbSet;

        public DokumentMagazynowyRepository(DatabaseClientEntities context) : base(context)
        {
            this.context = context;
            dbSet = context.Set<DatabaseClientEntities>();
        }

        public DaneOdbiorcyModel GetComplexById(decimal id)
        {
         
            var currentDokMag = Context.DOKUMENT_MAGAZYNOWY.FirstOrDefault(x => x.ID_DOK_MAGAZYNOWEGO == id);

            if (currentDokMag == null)
                return new DaneOdbiorcyModel();

            var complex = new DaneOdbiorcyModel
            {
                NumerDokumentu = currentDokMag.NUMER,
                IdDokumentuMagazynowego = currentDokMag.ID_DOK_MAGAZYNOWEGO,
                AdresOdbiorcy = MiejsceDostawyController.GetAdresByIdDokMag(currentDokMag.ID_DOK_MAGAZYNOWEGO,currentDokMag.ID_KONTRAHENTA ?? 0),
                AdresNadawcy = FirmaController.GetById(GlobalSettingsSingletonController.Instance.SelectedDaneNadawcy),
                WartoscBrutto = currentDokMag.WARTOSC_BRUTTO ?? 0
              
            };
          
            return complex;
        }
    }
}