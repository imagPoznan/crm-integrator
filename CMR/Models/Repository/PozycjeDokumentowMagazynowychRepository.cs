﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using EmptyProject.Models.Models;

namespace EmptyProject.Models.Repository
{
    internal class PozycjeDokumentowMagazynowychRepository : Repository<POZYCJA_DOKUMENTU_MAGAZYNOWEGO>
    {
        public DatabaseClientEntities context;
        public DbSet<DatabaseClientEntities> dbSet;

        public PozycjeDokumentowMagazynowychRepository(DatabaseClientEntities context) : base(context)
        {
            this.context = context;
            dbSet = context.Set<DatabaseClientEntities>();
        }


        public List<ListaTowarowModel> GetListaTowarowById(decimal id)
        {
            
            return (from pozycjaDokumentuMagazynowego in Context.POZYCJA_DOKUMENTU_MAGAZYNOWEGO
                join artykul in Context.ARTYKULs on pozycjaDokumentuMagazynowego.ID_ARTYKULU equals artykul.ID_ARTYKULU
                    where pozycjaDokumentuMagazynowego.ID_DOK_MAGAZYNOWEGO == id
                    select new ListaTowarowModel
                {
                    CechyINumery = artykul.NAZWA,
                    IloscSztuk = pozycjaDokumentuMagazynowego.ILOSC,
                    NrStatystyczny = pozycjaDokumentuMagazynowego.NR_SERII,
                    Objetosc = artykul.WYMIAR_G*artykul.WYMIAR_S*artykul.WYMIAR_W,
                    RodzajTowaru = pozycjaDokumentuMagazynowego.RODZAJ_ARTYKULU,
                    SposobOpakowania ="",
                    WagaBrutto = artykul.WAGA + "" + artykul.JED_WAGI,
                }).ToList();
        }

        public List<ARTYKUL> GetArtykulyByIdDokMag(decimal id)
        {

            return (from pozycjaDokumentuMagazynowego in Context.POZYCJA_DOKUMENTU_MAGAZYNOWEGO
                    join artykul in Context.ARTYKULs on pozycjaDokumentuMagazynowego.ID_ARTYKULU equals artykul.ID_ARTYKULU
                    where pozycjaDokumentuMagazynowego.ID_DOK_MAGAZYNOWEGO == id
                    select artykul).ToList();
        }

    }
}