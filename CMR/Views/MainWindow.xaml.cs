﻿using System.Windows;
using EmptyProject.Controllers;
using EmptyProject.Models.Models;
using Microsoft.Win32;

namespace EmptyProject.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        
        }

        private void tabControl_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            
        }
        
        private void cbDatabaseAuth_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            var selectBox = CbDatabaseAuth.SelectionBoxItem.ToString();
            if (selectBox != null && selectBox.Contains("SQL"))
            {
                TxtDatabaseLogin.IsEnabled = false;
                TxtDatabaseLogin.Text = string.Empty;
                TxtDatabasePassword.IsEnabled = false;
                TxtDatabasePassword.Text = string.Empty;
             
            }
            else
            {
                TxtDatabaseLogin.IsEnabled = true;
                TxtDatabasePassword.IsEnabled = true;
            }
        }
        
        #region AutoGeneratingColumn
        //private void dgFvList_AutoGeneratingColumn(object sender,
        //    System.Windows.Controls.DataGridAutoGeneratingColumnEventArgs e)
        //{
        //    switch (e.PropertyName)
        //    {
        //        case "Id":
        //            e.Column.Header = "Id szablonu";
        //            break;
        //        case "Name":
        //            e.Column.Header = "Nazwa";
        //            break;
        //        case "Kraj":
        //        case "SzablonHtml":
        //        case "SzablonTxt":
        //        case "Monit":
        //            e.Column.Visibility = Visibility.Hidden;
        //            break;
        //        case "KrajeString":
        //            e.Column.Header = "Wybrane kraje";
        //            break;
        //        case "Wlaczony":
        //            e.Column.Header = "Włączony";
        //            break;
        //        case "MonitString":
        //            e.Column.Header = "Monit";
        //            break;
        //        case "TematWiadomosci":
        //            e.Column.Header = "Temat wiadomości";
        //            break;
        //    }
        //}
        
     
        #endregion

        #region LoadingRows
      

        #endregion

      


       
    }
}
