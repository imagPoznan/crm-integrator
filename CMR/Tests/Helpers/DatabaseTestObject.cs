﻿using EmptyProject.Controllers;
using EmptyProject.Models.Repository;

namespace EmptyProject.Tests.Helpers
{
    public static class DatabaseTestObject
    {
        //ADD

        public static decimal AddTestKontrahent(KONTRAHENT kontrahentObj)
        {
            var kontrahent = new KontrahentRepository(new DatabaseClientEntities(DataBaseController.Instance.DbConnection()));

            kontrahent.Insert(kontrahentObj);

            return kontrahentObj.ID_KONTRAHENTA;
        }

        public static decimal AddTestDokHandl(DOKUMENT_HANDLOWY dokHandl)
        {
            var fv = new DokumentHandlowyRepository(new DatabaseClientEntities(DataBaseController.Instance.DbConnection()));

            fv.Insert(dokHandl);

            return dokHandl.ID_DOKUMENTU_HANDLOWEGO;
        }

        public static decimal AddTestMiejsceDostawy(MIEJSCE_DOSTAWY miejsceDostawyObiect)
        {
            var miejsceDostawyRepo = new MiejsceDostawyRepository(new DatabaseClientEntities(DataBaseController.Instance.DbConnection()));

            miejsceDostawyRepo.Insert(miejsceDostawyObiect);
        
            return miejsceDostawyObiect.ID_MIEJSCA_DOSTAWY;
        }

        public static DOSTAWA AddTestDostawa(DOSTAWA dostawa)
        {
            var dostawaRepo = new DostawaRepository(new DatabaseClientEntities(DataBaseController.Instance.DbConnection()));

            dostawaRepo.Insert(dostawa);

            return dostawa;
        }

        public static decimal AddTestDokMag(DOKUMENT_MAGAZYNOWY dokMag)
        {
            var dokMagRepo = new DokumentMagazynowyRepository(new DatabaseClientEntities(DataBaseController.Instance.DbConnection()));

            dokMagRepo.Insert(dokMag);

            return dokMag.ID_DOK_MAGAZYNOWEGO;
        }


        //DEL

        public static bool DeleteTestMiejsceDostawy(MIEJSCE_DOSTAWY miejsceDostawyObiect)
        {
            var miejsceDostawyRepo = new MiejsceDostawyRepository(new DatabaseClientEntities(DataBaseController.Instance.DbConnection()));
            var findToDel = miejsceDostawyRepo.GetById(miejsceDostawyObiect.ID_MIEJSCA_DOSTAWY);
            return miejsceDostawyRepo.Delete(findToDel);
        }

        public static bool DeleteTestKontrahent(KONTRAHENT kontrahent)
        {
            var kontrahentRepo = new KontrahentRepository(new DatabaseClientEntities(DataBaseController.Instance.DbConnection()));
            var findToDel = kontrahentRepo.GetById(kontrahent.ID_KONTRAHENTA);
            return kontrahentRepo.Delete(findToDel);
        }

        public static bool DeleteTestDokMagy(DOKUMENT_MAGAZYNOWY dokMag)
        {
            var dokMagRepo = new DokumentMagazynowyRepository(new DatabaseClientEntities(DataBaseController.Instance.DbConnection()));
            var findToDel = dokMagRepo.GetById(dokMag.ID_DOK_MAGAZYNOWEGO);
            return dokMagRepo.Delete(findToDel);
        }

        public static bool DeleteTestDostawa(DOSTAWA dostawaForTest)
        {
            var repo = new DostawaRepository(new DatabaseClientEntities(DataBaseController.Instance.DbConnection()));
            var findToDel = repo.GetById(dostawaForTest.ID_DOSTAWY);
            return repo.Delete(findToDel);
        }
    }
}
