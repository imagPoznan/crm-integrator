﻿using System;
using EmptyProject.Controllers;
using NUnit.Framework;

namespace EmptyProject.Tests
{
    [TestFixture]
    internal class LicenseTest
    {
        [Test]
        public void GetLicenseTest()
        {
            LicenseManagerController.LicencjaType licencjaType;
            DateTime? dataWygasniecia;

            Assert.AreEqual(LicenseManagerController.GetLicense(GlobalSettingsSingletonController.Instance.NumerLicencji, out licencjaType, out dataWygasniecia), true);

            Assert.AreEqual(LicenseManagerController.GetLicense("", out licencjaType, out dataWygasniecia), false);

        }
    }
}
